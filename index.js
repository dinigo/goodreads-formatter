const csv = require('fast-csv');
const [,,csvFile] = process.argv;

async function main(){
  const books = await new Promise((resolve, reject) => {
    const books = [];
    csv.
      fromPath(csvFile, {headers: true}).
      on('data', (data) => {books.push(data)}).
      on('end', () => {resolve(books)}).
      on('error', reject)
  });
  const processed = books.
    filter(book => book['Exclusive Shelf'] === 'read').
    map(book => `update: ${book.Title}; ${book.Author}; ; ${book['My Rating'] * 10/5}`).
    join('\n')
  console.log(processed);
}


main();
