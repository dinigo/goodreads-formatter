# FORMATTER FOR SRUIZ DASHBOARD

1. Install deps with `npm install`
2. Export your [library](https://www.goodreads.com/review/import)
3. Run the script `npm format myexport.csv` or `node index export.csv`
